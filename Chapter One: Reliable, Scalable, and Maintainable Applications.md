# Chapter One: Reliable, Scalable, and Maintainable Applications

- The chapter explores the fundamentals of what we are trying to achieve: **reliable, scalable and maintainable data systems**.
- Many applications today are data-intensive, as opposed to compute-intensive. Raw CPU power is rarely a limiting factor for these applications—bigger problems are usually the amount of data, the complexity of data, and the speed at which it is changing.
- There are many factors that may influence the design of a data system, including the skills and experience of the people involved, legacy system dependencies, the timescale for delivery, your organization’s tolerance of different kinds of risk, regulatory constraints, etc.
- **Reliability**: The system should continue to work correctly (performing the correct function at
the desired level of performance) even in the face of adversity (hardware or software
faults, and even human error).
- **Scalability**: As the system grows (in data volume, traffic volume, or complexity), there should
be reasonable ways of dealing with that growth. It is used to describe a system’s ability to cope with increased load.
- **Maintainability**: Over time, many different people will work on the system (engineering and operations,
both maintaining current behavior and adapting the system to new use cases), and they should all be able to work on it productively.

## Reliability
- A **fault** is usually defined as one component of the system deviating from its spec, whereas a **failure** is when the system as a whole stops providing the required service to the user. It is impossible to reduce the
probability of a fault to zero; therefore it is usually best to **design fault-tolerance mechanisms that prevent faults from causing failures.**

### Hardware Faults
- When we think of causes of system failure, hardware faults quickly come to mind. Hard disks crash, RAM becomes faulty, the power grid has a blackout, someone unplugs the wrong network cable.
- Our first response is usually to add redundancy to the individual hardware components in order to reduce the failure rate of the system. Disks may be set up in a RAID configuration, servers may have dual power supplies and hot-swappable CPUs, and datacenters may have batteries and diesel generators for backup power. When one
component dies, the redundant component can take its place while the broken component is replaced.

the failure rate of the system. Disks may be set up in a RAID configuration, servers may have dual power supplies and hot-swappable CPUs, and datacenters may have batteries and diesel generators for backup power. When one
component dies, the redundant component can take its place while the broken component is replaced.

### System Faults
- We usually think of hardware faults as being random and independent from each other: one machine’s disk failing does not imply that another machine’s disk is going to fail. There may be weak correlations (for example due to a common cause, such as the temperature in the server rack), but otherwise it is unlikely that a large number of hardware components will fail at the same time. But system faults are harder to anticipate, and because they are correlated across nodes, they tend to cause any more system failures than uncorrelated hardware faults. 
- There is no quick solution to the problem of systematic faults in software. Lots of small things can help: carefully thinking about assumptions and interactions in the system; thorough testing; process isolation; allowing processes to crash and restart; measuring, monitoring, and analyzing system behavior in production.

### Human Errors
- Humans design and build software systems, and the operators who keep the systems running are also human. Even when they have the best intentions, humans are known to be unreliable.
- How do we make our systems reliable, in spite of unreliable humans? The best systems
combine several approaches:
    - Decouple the places where people make the most mistakes from the places where they can cause failures. In particular, provide fully featured non-production sandbox environments where people can explore and experiment safely, using real data, without affecting real users.
    - Allow quick and easy recovery from human errors, to minimize the impact in the case of a failure. For example, make it fast to roll back configuration changes, roll out new code gradually (so that any unexpected bugs affect only a small subset of users).
    - Set up detailed and clear monitoring, such as performance metrics and error rates.
    
### How important is Reliabilty?
- Slow is the new down. In today’s lightening speed and competitive digital market, if a customer finds that your application is slow or doesn’t load quickly, then you can expect them to pack up and head towards the competitor.

## Scalability
How do we maintain good performance even when our load parameters increase by some amount?

### Describing Load
- Load can be described with a few numbers which we call load parameters. The best choice of parameters
depends on the architecture of your system: it may be requests per second to a web server, the ratio of reads to writes in a database, the number of simultaneously active users in a chat room, the hit rate on a cache, or something else.


### Describing Performance
- Once you have described the load on your system, you can investigate what happens when the load increases. You can look at it in two ways:
    - When you increase a load parameter and keep the system resources (CPU, memory, network bandwidth, etc.) unchanged, how is the performance of your system affected?
    - When you increase a load parameter, how much do you need to increase the resources if you want to keep performance unchanged?
- **Latency vs Response time**: Latency and response time are often used synonymously, but they are not the same. The response time is what the client sees: besides the actual time to process the request (the service time), it includes network delays and queueing delays. Latency is the duration that a request is waiting to be handled—during which it is latent, awaiting service.
- **Percentiles** are often used in _**service level objectives (SLOs)**_ and _**service level agreements (SLAs)**_, contracts that define the expected performance and availability of a service. An SLA may state that the service is considered to be up if it has a median response time of less than 200 ms and a 99th percentile under 1 s (if the response time is longer, it might as well be down), and the service may be required to be up at least 99.9% of the time.
- **Queueing delays** often account for a large part of the response time at high percentiles. As a server can only process a small number of things in parallel (limited, for example, by its number of CPU cores), it only takes a small number of slow requests to hold up the processing of subsequent requests—an effect sometimes known as _**head-of-line blocking**_. Even if those subsequent requests are fast to process on the server, the client will see a slow overall response time due to the time waiting for the prior request to complete.

### Approaches for Coping with Load
- The architecture of systems that operate at large scale is usually highly specific to the
application—there is no such thing as a generic, one-size-fits-all scalable architecture
(informally known as **magic scaling sauce**). The architecture depends on the load parameter you are focusing.  
- People often talk of a dichotomy between **scaling up** (vertical scaling, moving to a more powerful machine) and **scaling out** (horizontal scaling, distributing the load across multiple smaller machines). In reality, good architectures usually involve a pragmatic mixture of approaches: for example, using several fairly powerful
machines can still be simpler and cheaper than a large number of small virtual machines.
- Some systems are **elastic**, meaning that they can automatically add computing resources when they detect a load increase, whereas other systems are scaled manually (a human analyzes the capacity and decides to add more machines to the system).
- While distributing **stateless services** across multiple machines is fairly straightforward, taking **stateful data systems** from a single node to a distributed setup can introduce a lot of additional complexity.

## Maintainability
- Making a system simpler does not necessarily mean reducing its functionality; it can also mean removing **accidental complexity**. Complexity is defined as accidental if it is not inherent in the problem that the software solves (as seen by the users) but arises only from the implementation.
